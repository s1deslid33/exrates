﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ExRatesWeb.Data;
using ExRatesWeb.Data.Models;
using Newtonsoft.Json;
using ExRatesWeb.Services;

namespace ExRatesWeb.Controllers
{
    public class ExchageRatesController : Controller
    {
        private readonly AppDbContext _context;
        private readonly ExchageRatesServices _services;
        private readonly string bankApiUrl = "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json";

        public ExchageRatesController(AppDbContext context, ExchageRatesServices services)
        {
            _context = context;
            _services = services;
        }

        // GET: ExchageRates
        public async Task<IActionResult> Index()
        {
            return View(await _context.ExchangeRates.ToListAsync());
        }

        public async Task<IActionResult> ListWithTimeInterval([FromQuery] string from, string to)
        {
            TimeSpan timeFrom;
            TimeSpan timeTo;

            TimeSpan.TryParse(from, out timeFrom);
            TimeSpan.TryParse(to, out timeTo);

            List<ExchageRates> editedExRates = new List<ExchageRates>();

            await foreach (var item in _context.ExchangeRates)
            {
                if(item.Datetime.TimeOfDay >= timeFrom && item.Datetime.TimeOfDay <= timeTo && item.Datetime.Date == DateTime.Now.Date)
                    editedExRates.Add(item);
            }

            return View(editedExRates);
        }

        public async Task<IActionResult> GetLatestExRates()
        {
            int _id;

            _services.GetLatestExRates();

            _id = _context.ExchangeRates.OrderByDescending(x => x.Id).FirstOrDefault().Id;

            return RedirectToAction("Details", new { id = _id });
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var exchageRates = await _context.ExchangeRates
                .FirstOrDefaultAsync(m => m.Id == id);
            if (exchageRates == null)
            {
                return NotFound();
            }

            return View(exchageRates);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Usd,Eur,Rub,Datetime")] ExchageRates exchageRates)
        {
            if (ModelState.IsValid)
            {
                _context.Add(exchageRates);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(exchageRates);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var exchageRates = await _context.ExchangeRates.FindAsync(id);
            if (exchageRates == null)
            {
                return NotFound();
            }
            return View(exchageRates);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Usd,Eur,Rub,Datetime")] ExchageRates exchageRates)
        {
            if (id != exchageRates.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(exchageRates);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ExchageRatesExists(exchageRates.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(exchageRates);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var exchageRates = await _context.ExchangeRates
                .FirstOrDefaultAsync(m => m.Id == id);
            if (exchageRates == null)
            {
                return NotFound();
            }

            return View(exchageRates);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var exchageRates = await _context.ExchangeRates.FindAsync(id);
            _context.ExchangeRates.Remove(exchageRates);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ExchageRatesExists(int id)
        {
            return _context.ExchangeRates.Any(e => e.Id == id);
        }
    }
}
