﻿using ExRatesWeb.Models;
using Microsoft.EntityFrameworkCore;
using System;
using ExRatesWeb.Data.Models;

namespace ExRatesWeb.Data
{
    public class AppDbContext : DbContext
    {
        public virtual DbSet<ExchageRates> ExchangeRates { get; set; }
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ExchageRates>(entity =>
            {
                entity.ToTable("exchagerates");

                entity.Property(x => x.Id).HasColumnName("id");

                entity.Property(x => x.Datetime).HasColumnName("datetime");

                entity.Property(x => x.Eur).HasColumnName("eur").IsRequired().HasMaxLength(50);

                entity.Property(x => x.Usd).HasColumnName("usd").IsRequired().HasMaxLength(50);

                entity.Property(x => x.Rub).HasColumnName("rub").IsRequired().HasMaxLength(50);
            });

        }

        public DbSet<ExchageRates> ExchageRates { get; set; }

    }
}
