﻿namespace ExRatesWeb.Data.Models
{
    public class BankInfoJson
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Rate { get; set; }
        public string CC { get; set; }
        public string ExDate { get; set; }
    }
}
