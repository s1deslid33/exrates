﻿namespace ExRatesWeb.Data.Models
{
    public class ExchageRates
    {
        public int Id { get; set; }
        public string Usd { get; set; }
        public string Eur { get; set; }
        public string Rub { get; set; }
        public DateTime Datetime { get; set; }
    }
}
