﻿using ExRatesWeb.Controllers;
using ExRatesWeb.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Drawing;
using System.Net;
using System.Web.Mvc;
using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Extensions.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.InputFiles;
using Telegram.Bot.Types.ReplyMarkups;
using WkHtmlWrapper.Image.Converters;

namespace ExRatesWeb.Data.Models
{
    public class Bot
    {
        private TelegramBotClient _client;
        private readonly AppDbContext _context;
        private ReplyKeyboardMarkup _keyboard;
        ExchageRatesServices _service;
        private readonly string _BotToken = "5151300247:AAFjYiCbnuo5dsRp5K-YpL3iwYVhDqVQZkU";
        private TimeSpan timeFrom;
        private TimeSpan timeTo;

        private string lastCommand;

        public Bot(AppDbContext context, ExchageRatesServices service)
        {
            _service = service;
            _context = context;
            timeFrom = timeTo = default;
            lastCommand = string.Empty;
            CreateKeyboard();
        }

        private void CreateKeyboard()
        {
            var keyboard = new List<IEnumerable<KeyboardButton>>();

            keyboard.Add(new KeyboardButton[] { new KeyboardButton("Get current exchange rate") });
            keyboard.Add(new KeyboardButton[] { new KeyboardButton("Get exchange rate history") });
            keyboard.Add(new KeyboardButton[] { new KeyboardButton("List exchange rate for todays time interval") });

            _keyboard = new ReplyKeyboardMarkup(keyboard);
            _keyboard.ResizeKeyboard = true;
        }

        public void BotStart()
        {
            _client = new TelegramBotClient(_BotToken);

            using var cts = new CancellationTokenSource();

            var receiverOptions = new ReceiverOptions
            {
                AllowedUpdates = { }
            };
            _client.StartReceiving(
                HandleUpdateAsync,
                HandleErrorAsync,
                receiverOptions,
                cancellationToken: cts.Token);

            var me = _client.GetMeAsync().Result;
            Console.ReadLine();

            cts.Cancel();
        }
        private void SendPhotoMessage(long chatId, string text, string photo)
        {
            try
            {
                using (var stream = System.IO.File.Open(photo, FileMode.Open))
                {
                    var res = _client.SendPhotoAsync(chatId, new InputOnlineFile(stream, photo), text);
                    res.Wait();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message + Environment.NewLine + e.StackTrace);
            }
        }

        private void SendMessage(ITelegramBotClient botClient, string command, long chatId)
        {
            try
            {
                if (command == "Get current exchange rate")
                {
                    _service.GetLatestExRates();
                    var path = GetCurExRate();
                    if (path == null)
                        throw new Exception();
                    botClient.SendTextMessageAsync(chatId, "Current exchange rate:", replyMarkup: _keyboard);
                    SendPhotoMessage(chatId, "", path);
                    DeletePhoto(path);
                    return;
                }
                if (command == "Get exchange rate history")
                {
                    var path = GetExRatesList(true);
                    if (path == null)
                        throw new Exception();
                    botClient.SendTextMessageAsync(chatId, "Exchange rates list:", replyMarkup: _keyboard);
                    SendPhotoMessage(chatId, "", path);
                    DeletePhoto(path);
                    return;
                }
                if (command == "List exchange rate for todays time interval")
                {
                    lastCommand = command;
                    botClient.SendTextMessageAsync(chatId, "Input time From (HH:mm:ss) :", replyMarkup: _keyboard);
                    return;
                }
                if (!string.IsNullOrEmpty(command) && lastCommand != string.Empty && timeFrom == default)
                {
                    if (!TimeSpan.TryParse(command, out timeFrom))
                    {
                        botClient.SendTextMessageAsync(chatId, "Wrong input type. Try again");
                        botClient.SendTextMessageAsync(chatId, "Input time From (HH:mm:ss) like 00:00:00 :");
                        return;
                    }

                    botClient.SendTextMessageAsync(chatId, "Input time To (HH:mm:ss) :");
                    return;
                }
                if (!string.IsNullOrEmpty(command) && lastCommand != string.Empty && timeFrom != default)
                {
                    if (!TimeSpan.TryParse(command, out timeTo))
                    {
                        botClient.SendTextMessageAsync(chatId, "Wrong input type. Try again");
                        botClient.SendTextMessageAsync(chatId, "Input time To (HH:mm:ss) like 00:00:00 :");
                        return;
                    }

                    var path = GetExRatesList(false);
                    if (path == null)
                        throw new Exception();
                    botClient.SendTextMessageAsync(chatId, $"Exchange rates list from {timeFrom} to {timeTo}:", replyMarkup: _keyboard);
                    SendPhotoMessage(chatId, "", path);
                    DeletePhoto(path);
                    timeFrom = timeTo = default;
                    lastCommand = string.Empty;
                    return;

                }
                if (!string.IsNullOrEmpty(command) && command == "/start")
                {
                    botClient.SendTextMessageAsync(chatId, "Hello\nSelect menu item:", replyMarkup: _keyboard);
                    return;
                }
                else
                {
                    botClient.SendTextMessageAsync(chatId, "Wrong data input. Please try again", replyMarkup: _keyboard);
                    return;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message, e.StackTrace);
            }
        }
        private void DeletePhoto(string path)
        {
            try
            {
                System.IO.File.Delete(path);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message + Environment.NewLine + e.StackTrace);
            }
        }

        public string GetExRatesList(bool isFoolList)
        {
            try
            {
                List<ExchageRates> exRates;

                if (isFoolList)
                    exRates = _service.ExchangeRatesList;
                else
                {
                    exRates = _service.ListForTimeInterval(timeFrom, timeTo);
                }

                HtmlToImageConverter converter = new HtmlToImageConverter();
                string filePath = $"/temp/{DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds.ToString()}.jpg";
                string html = "";

                using (var sr = new StreamReader(@"ExRates.html"))
                {
                    html = sr.ReadToEnd();
                    sr.Dispose();
                }
                 
                string result = Path.Combine(("/wwwroot" + filePath).Split('\\', '/'));
                var table = "";

                foreach (var item in exRates)
                {
                    table += $"<tr class='center'><td>{item.Usd}</td><td>{item.Eur}</td><td>{item.Rub}</td><td>{item.Datetime}</td></tr>";
                }
                html = html.Replace("mytable", table);

                converter.ConvertAsync(html, result).Wait();
                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message + Environment.NewLine + e.StackTrace);
                return null;
            }

        }

        private string GetCurExRate()
        {
            try
            {
                ExchageRates item = _service.ExchangeRatesLastItem;
                HtmlToImageConverter converter = new HtmlToImageConverter();
                string filePath = $"/temp/{DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds.ToString()}.jpg";
                string html = "";

                using (var sr = new StreamReader(@"CurrentExRate.html"))
                {
                    html = sr.ReadToEnd();
                    sr.Dispose();
                }

                string result = Path.Combine(("/wwwroot" + filePath).Split('\\', '/'));

                var table = $"<dt class = 'col-sm-2'>USD</dt><dd class = 'col-sm-10'>{item.Usd}</dd>";
                table += $"<dt class = 'col-sm-2'>EUR</dt><dd class = 'col-sm-10'>{item.Eur}</dd>";
                table += $"<dt class = 'col-sm-2'>RUB</dt><dd class = 'col-sm-10'>{item.Rub}</dd>";
                table += $"<dt class = 'col-sm-2'>Date and time</dt><dd class = 'col-sm-10'>{item.Datetime}</dd>";

                html = html.Replace("mytable", table);

                converter.ConvertAsync(html, result).Wait();

                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message + Environment.NewLine + e.StackTrace);
                return null;
            }

        }

        private async Task HandleUpdateAsync(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
        {
            try
            {

                if (update.Type != UpdateType.Message)
                    return;

                if (update.Message!.Type != MessageType.Text)
                    return;

                var chatId = update.Message.Chat.Id;
                var messageText = update.Message.Text;



                SendMessage(botClient, update.Message.Text, chatId);


            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message, e.StackTrace);
            }
        }

        private Task HandleErrorAsync(ITelegramBotClient botClient, Exception exception, CancellationToken cancellationToken)
        {
            var ErrorMessage = exception switch
            {
                ApiRequestException apiRequestException
                    => $"Telegram API Error:\n[{apiRequestException.ErrorCode}]\n{apiRequestException.Message}",
                _ => exception.ToString()
            };


            return Task.CompletedTask;
        }
    }
}
