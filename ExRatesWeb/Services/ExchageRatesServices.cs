﻿using ExRatesWeb.Data;
using ExRatesWeb.Data.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace ExRatesWeb.Services
{
    public class ExchageRatesServices
    {
        private readonly AppDbContext _context;
        private readonly string bankApiUrl = "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json";
        private List<ExchageRates> editedExRates = new();
        public List<ExchageRates> ExchangeRatesList { get => _context.ExchageRates.ToList(); }
        public ExchageRates ExchangeRatesLastItem { get => _context.ExchangeRates.OrderByDescending(x => x.Id).FirstOrDefault(); }

        public ExchageRatesServices(AppDbContext context)
        {
            _context = context;
        }

        public List<ExchageRates> ListForTimeInterval(TimeSpan timeFrom, TimeSpan timeTo)
        {
            List<ExchageRates> exRates = new List<ExchageRates>();

            foreach (var item in _context.ExchangeRates)
            {
                if (item.Datetime.TimeOfDay >= timeFrom && item.Datetime.TimeOfDay <= timeTo && item.Datetime.Date == DateTime.Now.Date)
                    exRates.Add(item);
            }

            return exRates;
        }

        public int ListWithTimeInterval(string from, string to)
        {
            try
            {
                TimeSpan timeFrom;
                TimeSpan timeTo;

                TimeSpan.TryParse(from, out timeFrom);
                TimeSpan.TryParse(to, out timeTo);

                editedExRates.RemoveRange(0, editedExRates.Count);

                foreach (var item in _context.ExchangeRates)
                {
                    if (item.Datetime.TimeOfDay >= timeFrom && item.Datetime.TimeOfDay <= timeTo && item.Datetime.Date == DateTime.Now.Date)
                        editedExRates.Add(item);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return StatusCodes.Status500InternalServerError;
            }
            return StatusCodes.Status200OK;
        }

        public void GetLatestExRates()
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    var json = httpClient.GetStringAsync(bankApiUrl);

                    json.Wait();

                    List<BankInfoJson> response = JsonConvert.DeserializeObject<List<BankInfoJson>>(json.Result);

                    var BankData = response.Where(x => x.CC == "USD" || x.CC == "EUR" || x.CC == "RUB").ToList();

                    _context.ExchangeRates.Add(new ExchageRates()
                    {
                        Datetime = DateTime.Now,
                        Eur = BankData.FirstOrDefault(x => x.CC == "EUR").Rate,
                        Usd = BankData.FirstOrDefault(x => x.CC == "USD").Rate,
                        Rub = BankData.FirstOrDefault(x => x.CC == "RUB").Rate
                    });

                    _context.SaveChanges();
                }
            } catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                //return StatusCodes.Status500InternalServerError;
            }
            //return StatusCodes.Status200OK;
        }

        public int Details(int? id)
        {
            if (id == null)
            {
                return StatusCodes.Status404NotFound;
            }

            var exchageRates = _context.ExchangeRates
                .FirstOrDefaultAsync(m => m.Id == id);
            if (exchageRates == null)
            {
                return StatusCodes.Status500InternalServerError;
            }

            return StatusCodes.Status200OK;
        }

        public int Create(ExchageRates exchageRates)
        {
            if(exchageRates != null)
            {
                _context.Add(exchageRates);
                _context.SaveChanges();
                return StatusCodes.Status200OK;
            }
            return StatusCodes.Status500InternalServerError;
        }

        public int Edit(int? id)
        {
            if (id == null)
            {
                return StatusCodes.Status404NotFound;
            }

            var exchageRates = _context.ExchangeRates.Find(id);
            if (exchageRates == null)
            {
                return StatusCodes.Status500InternalServerError;
            }
            return StatusCodes.Status200OK;
        }

        public int Edit(int id, ExchageRates exchageRates)
        {
            if (id != exchageRates.Id)
            {
                return StatusCodes.Status404NotFound;
            }

            if (exchageRates != null)
            {
                try
                {
                    _context.Update(exchageRates);
                    _context.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ExchageRatesExists(exchageRates.Id))
                    {
                        return StatusCodes.Status404NotFound;
                    }
                    else
                    {
                        throw;
                    }
                }
                return StatusCodes.Status200OK;
            }
            return StatusCodes.Status200OK;
        }

        public int Delete(int? id)
        {
            if (id == null)
            {
                return StatusCodes.Status404NotFound;
            }

            var exchageRates = _context.ExchangeRates
                .FirstOrDefaultAsync(m => m.Id == id);
            if (exchageRates == null)
            {
                return StatusCodes.Status404NotFound;
            }

            return StatusCodes.Status200OK;
        }

        public int DeleteConfirmed(int id)
        {
            var exchageRates = _context.ExchangeRates.Find(id);
            _context.ExchangeRates.Remove(exchageRates);
            _context.SaveChanges();
            return StatusCodes.Status200OK;
        }

        private bool ExchageRatesExists(int id)
        {
            return _context.ExchangeRates.Any(e => e.Id == id);
        }
    }
}
