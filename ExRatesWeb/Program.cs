using ExRatesWeb.Controllers;
using ExRatesWeb.Data;
using ExRatesWeb.Data.Models;
using ExRatesWeb.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

var builder = WebApplication.CreateBuilder(args);
var ConnectionString = builder.Configuration.GetConnectionString("DefaultConnection");

// Add services to the container.
builder.Services.AddControllersWithViews();
builder.Services.AddDbContext<AppDbContext>(options => options.UseNpgsql(ConnectionString));
builder.Services.AddTransient<ExchageRatesServices>();
AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);

var app = builder.Build();

new Thread(delegate () { BotStart(); }).Start();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();


void BotStart()
{
    //var optBuild  = new optionBuil
    Bot bot;
    var optionBuilder = new DbContextOptionsBuilder<AppDbContext>();
    optionBuilder.UseNpgsql(builder.Configuration.GetConnectionString("DefaultConnection"));

    var context = new AppDbContext(optionBuilder.Options);
    bot = new Bot(context, app.Services.CreateScope().ServiceProvider.GetRequiredService<ExchageRatesServices>());
    bot.BotStart();
}
